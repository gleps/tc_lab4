%% init
clear all;
fs = 1e3;
dt = 1/fs;
t = 0:dt:1;

M = 1; % modulation depth

car_freq = 1e2;
car_omega = 2*pi*car_freq;
car_phi = 0;
car_sig = cos(car_omega*t + car_phi);

inf_freq = 10;
inf_omega = 2*pi*inf_freq;
inf_sig = cos(inf_omega*t);
shift = 1;

%% inf_sig
plot_range = 1:500;
figure;
img = plot(t(plot_range), inf_sig(plot_range));
saveas(img, 'report/fig/inf_sig.png');

%% amplitude modulation

mod_sig = (shift + M*inf_sig).*car_sig;
plot_range = 1:200;
figure;
img = plot(t(plot_range), mod_sig(plot_range));
saveas(img, 'report/fig/mod_sig1.png');

M = 2;

mod_sig = (shift + M*inf_sig).*car_sig;
plot_range = 1:200;
figure;
img = plot(t(plot_range), mod_sig(plot_range));
saveas(img, 'report/fig/mod_sig2.png');

M = 0.2;

mod_sig = (shift + M*inf_sig).*car_sig;
plot_range = 1:200;
figure;
img = plot(t(plot_range), mod_sig(plot_range));
saveas(img, 'report/fig/mod_sig0_2.png');

%% spectrum
Nfft = 2^nextpow2(length(mod_sig));
f = (0:Nfft-1)/Nfft*fs;

mod_sp = abs(fft(mod_sig, Nfft))/(Nfft/2);

figure;
lbound = round((car_freq - inf_freq*4)/fs*Nfft);
hbound = round((car_freq + inf_freq*4)/fs*Nfft);
img = plot(f(lbound:hbound), mod_sp(lbound:hbound));
xlabel('frequency');
ylabel('magnitude');
title('modulated signal spectrum');
saveas(img, 'report/fig/mod_sp.png');

%% with a suppressed carrier frequency
M = 1;
mod_sig = (M*inf_sig).*car_sig;

plot_range = 1:200;
img = plot(t(plot_range), mod_sig(plot_range));
saveas(img, 'report/fig/mod_sig_sup.png');

%% spectrum
Nfft = 2^nextpow2(length(mod_sig));
f = (0:Nfft-1)/Nfft*fs;

mod_sp = abs(fft(mod_sig, Nfft))/(Nfft/2);

figure;
lbound = round((car_freq - inf_freq*4)/fs*Nfft);
hbound = round((car_freq + inf_freq*4)/fs*Nfft);
img = plot(f(lbound:hbound), mod_sp(lbound:hbound));
xlabel('frequency');
ylabel('magnitude');
title('modulated signal spectrum');
saveas(img, 'report/fig/mod_sig_sup_sp.png');

%% single-sideband modulation
phi = 0;
mod_sig = car_sig + 0.5*M*(cos((car_omega + ...
    inf_omega)*t) + car_phi + phi);

plot_range = 1:200;
img = plot(t(plot_range), mod_sig(plot_range));
saveas(img, 'report/fig/mod_sig_ssb.png');

%% spectrum
Nfft = 2^nextpow2(length(mod_sig));
f = (0:Nfft-1)/Nfft*fs;

mod_sp = abs(fft(mod_sig, Nfft))/(Nfft/2);

figure;
lbound = round((car_freq - inf_freq*4)/fs*Nfft);
hbound = round((car_freq + inf_freq*4)/fs*Nfft);
img = plot(f(lbound:hbound), mod_sp(lbound:hbound));
xlabel('frequency');
ylabel('magnitude');
title('modulated signal spectrum');
saveas(img, 'report/fig/mod_sig_ssb_sp.png');

%% synchronous detection
ref_sig = cos(car_omega*t);
inf_sig_res = mod_sig.*ref_sig;

plot_range = 1:200;
img = plot(t(plot_range), inf_sig_res(plot_range));
saveas(img, 'report/fig/inf_sig_res.png');

%% spectrum
Nfft = 2^nextpow2(length(inf_sig_res));
f = (0:Nfft-1)/Nfft*fs;

ref_sp = abs(fft(inf_sig_res, Nfft))/(Nfft/2);

figure;
lbound = 1;
hbound = 500;
img = plot(f(lbound:hbound), ref_sp(lbound:hbound));
xlabel('frequency');
ylabel('magnitude');
title('modulated signal spectrum');
saveas(img, 'report/fig/ref_sp.png');

%%
inf_sig_res_inp.time = (0:dt:1)';
inf_sig_res_inp.signals.values = (inf_sig_res)';

sim('fltr');

%%
plot_range = 1:1000;
img = plot(t(plot_range), inf_sig_fil(plot_range));
saveas(img, 'report/fig/inf_sig_fil.png');

%%

M = 0:0.1:10;
n = M.^2./(M.^2 + 2);
plot(M, n);
